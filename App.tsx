import React from 'react'
import 'react-native-gesture-handler'
import { Provider } from 'react-redux'
import StackNavigator from './src/Navigation/index'

import store from './src/Redux/Store'

const App = () => {
  return (
    <Provider store={store}>
      <StackNavigator />
    </Provider>
  )
}

export default App
