import { combineReducers, createStore, applyMiddleware } from 'redux'
import { reducer } from './RootReducer'
import rootSaga from '../Sagas'
import createSagaMiddleware from 'redux-saga'

const sagaMiddleware = createSagaMiddleware()

/* ------------- Assemble The Reducers ------------- */
const reducers = combineReducers({
  cart: reducer
})

const store = createStore(reducers, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(rootSaga)
export default store
