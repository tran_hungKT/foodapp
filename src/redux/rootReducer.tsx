import { createReducer, createActions } from 'reduxsauce'
import { CartState, Element } from '../Types/RootState'

//Middleware function
function isProductIncludeInPackage(idProduct: object, packageProduct: any[]) {
  if (!packageProduct.includes(idProduct)) {
    return false
  }
  return true
}

//Type

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
  loadData: [],
  loadDataSuccess: ['res'],
  loadDataFail: ['message'],

  addItem: ['food'],
  increaseQuantity: ['id'],
  decreaseQuantity: ['id'],
  deleteItem: ['id']
})

export const ProductType = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITITALS_STATE: CartState = {
  datas: [],
  package: [],
  totalPrice: 0,
  totalQuantity: 0,
  errorMessage: ''
}

/* ------------- Reducers ------------- */
export const addItem = (state: CartState, action: { food: Element }) => {
  const { food } = action
  const newState = { ...state }
  if (isProductIncludeInPackage(food, newState.package)) {
    newState.package.find((element: Element) => {
      if (element.id == food.id) {
        element.quantity += 1
        newState.totalQuantity += 1
        newState.totalPrice += element.price
      }
    })
    return newState
  }
  food.quantity = 1
  newState.totalQuantity += 1
  newState.totalPrice += action.food.price
  newState.package = newState.package.concat(action.food)
  return newState
}

export const deleteItem = (state: CartState, action: { id: String }) => {
  const { id } = action
  let deleteItemIndex = 0
  const newState = { ...state }
  newState.package.find((element) => {
    if (element.id == id) {
      newState.totalQuantity -= element.quantity
      newState.totalPrice -= element.price * element.quantity
      deleteItemIndex = newState.package.indexOf(element)
    }
  })
  newState.package.splice(deleteItemIndex, 1)
  return newState
}

export const increaseQuantity = (state: CartState, action: { id: string }) => {
  const { id } = action
  const newState = { ...state }
  newState.package.find((element) => {
    if (element.id == id) {
      element.quantity += 1
      newState.totalQuantity += 1
      newState.totalPrice += element.price
    }
  })
  return newState
}

export const decreaseQuantity = (state: CartState, action: { id: string }) => {
  const { id } = action
  const newState = { ...state }

  newState.package.find((element) => {
    if (element.id == id) {
      if (element.quantity > 0) {
        element.quantity -= 1
        newState.totalQuantity -= 1
        newState.totalPrice -= element.price
      }
    }
  })
  return newState
}

export const loadData = (state: CartState) => {
  return {
    ...state
  }
}
export const loadDataFail = (state: CartState, action: { message: string }) => {
  return {
    ...state,
    errorMessage: action.message
  }
}
export const loadDataSuccess = (state: CartState, action: { res: [] }) => {
  const newState = { ...state }
  newState.datas = action.res
  return newState
}

export const reducer = createReducer(INITITALS_STATE, {
  [Types.ADD_ITEM]: addItem,
  [Types.DELETE_ITEM]: deleteItem,
  [Types.INCREASE_QUANTITY]: increaseQuantity,
  [Types.DECREASE_QUANTITY]: decreaseQuantity,
  [Types.LOAD_DATA]: loadData,
  [Types.LOAD_DATA_SUCCESS]: loadDataSuccess,
  [Types.LOAD_DATA_FAIL]: loadDataFail
})
