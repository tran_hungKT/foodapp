import { call, put } from 'redux-saga/effects'
import axios from 'axios'
import CartAction from '../Redux/RootReducer'
const api =
  'https://gist.githubusercontent.com/trandongtam/21b7633d121e6e72d1afcc603f484fe5/raw/f9e8558f62d854715fc63fc9eafaafb78d68e7c8/data.json'

async function getApi() {
  try {
    const datas = await axios.get(api)
    return datas
  } catch {
    console.log('Can not get data')
  }
}

export function* getFoodDatas() {
  try {
    const response = yield call(getApi)
    if (response.status == 200) {
      yield put(CartAction.loadDataSuccess(response.data))
    } else {
      yield put(CartAction.loadDataFail(response.message))
    }
  } catch (error) {
    yield put(CartAction.loadDataFail('serverError'))
  }
}
