import { takeLatest, all, takeEvery } from 'redux-saga/effects'

/* ------------- Types ------------- */
import { ProductType } from '../Redux/RootReducer'

import { getFoodDatas } from './GetDataFoodSagas'

export default function* root() {
  yield takeLatest(ProductType.LOAD_DATA, getFoodDatas)
}
