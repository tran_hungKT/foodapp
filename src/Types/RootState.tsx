export type Element = {
  id: string
  quantity: number
  name: string
  price: number
  image: string
  items: []
}
export type CartState = {
  datas: Element[]
  package: Element[]
  totalPrice: number
  totalQuantity: number
  errorMessage: string
}

type RootState = {
  cart: CartState
}
export default RootState
