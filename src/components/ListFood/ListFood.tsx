import React from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ImageBackground
} from 'react-native'
import styles from '../Styles/ListFoodStyles'
import { Element } from '../../Types/RootState'

type ListFoodProps = {
  dataItems: Element[]
  onPressItem: (item: Element) => void
}

const ListFood = (props: ListFoodProps) => {
  const listFood = props.dataItems.map((item, key) => {
    const source = !!item.image
      ? item.image
      : 'https://monngondongian.com/wp-content/uploads/2019/06/banh-mi-sandwich-cuc-thom-va-mem.png'
    return (
      <TouchableOpacity
        style={styles.listFoodContainer}
        key={key}
        onPress={() => props.onPressItem(item)}
      >
        <View style={styles.image}>
          <Image
            source={{
              uri: source
            }}
            style={styles.image}
          />
        </View>
        <View style={styles.contentFood}>
          <ImageBackground
            source={{
              uri: source
            }}
            resizeMethod={'resize'}
            style={styles.imageBack}
            imageStyle={styles.backgroundImage}
            blurRadius={10}
          >
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.price}>$ {item.price}</Text>
          </ImageBackground>
        </View>
      </TouchableOpacity>
    )
  })
  return <>{listFood}</>
}

export default ListFood
