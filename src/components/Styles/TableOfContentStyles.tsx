import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  seperateItem: {
    flex: 0.05,
    flexDirection: 'row',
    borderBottomColor: '#8C8C89',
    borderBottomWidth: 1.5,
    marginBottom: 10,
    marginTop: 10
  },
  itemsView: {
    flex: 0.4,
    alignContent: 'flex-start'
  },
  textItem: {
    fontWeight: 'bold',
    fontSize: 16
  },
  quantityView: {
    flex: 0.4,
    alignContent: 'flex-start',
    flexDirection: 'row'
    // justifyContent: 'space-between'
  },
  priceView: {
    flex: 0.2,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  }
})
