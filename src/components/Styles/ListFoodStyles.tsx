import { StyleSheet, Dimensions } from 'react-native'
const windowWidth = Dimensions.get('window').width
export default StyleSheet.create({
  listFoodContainer: {
    flexDirection: 'row',
    flex: 0.1,
    marginTop: 10,
    alignItems: 'center',
    borderColor: '#79FA6A',
    borderRadius: 10,
    borderWidth: 0.5
  },
  image: {
    flex: 1,
    width: 100,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10
  },
  contentFood: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1
  },
  name: {
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 5
  },
  price: {
    fontWeight: 'bold',
    color: '#EAD90B',
    fontSize: 18
  },
  backgroundImage: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10
  },
  imageBack: {
    width: windowWidth - 100 - 20,
    flex: 100,
    marginRight: 100,
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center'
  }
})
