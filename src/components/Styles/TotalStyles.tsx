import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  seperateItem: {
    flex: 0.1,
    flexDirection: 'row',
    borderBottomColor: '#B7C1B3',
    marginTop: 10
  },
  itemsView: {
    flex: 0.4,
    alignContent: 'center',
    marginRight: 0
  },
  textItem: {
    fontWeight: 'bold',
    fontSize: 16
  },
  quantityView: {
    flex: 0.4,
    alignContent: 'flex-start',
    flexDirection: 'row'
  },
  centerQuantity: {
    marginLeft: 25
  },
  priceView: {
    flex: 0.2,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  }
})
