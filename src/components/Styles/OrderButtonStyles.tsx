import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainContainer: {
    height: 50,
    width: 175,
    borderWidth: 2,
    borderColor: '#74746A',
    borderRadius: 10,
    alignContent: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  image: {
    width: 40,
    height: 40,
    tintColor: '#74746A'
  },
  text: {
    fontSize: 16,
    color: '#74746A',
    fontWeight: '700'
  }
})
