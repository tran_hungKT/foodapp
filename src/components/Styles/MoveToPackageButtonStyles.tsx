import { StyleSheet, Dimensions } from 'react-native'

const windowWidth = Dimensions.get('window').width

export default StyleSheet.create({
  mainContainer: {
    flexDirection: 'row',
    marginHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 60,
    borderColor: '#4FEA0B',
    borderWidth: 2,
    width: windowWidth - 80,
    alignSelf: 'center',
    padding: 0,
    borderRadius: 10
  },
  image: {
    height: 40,
    width: 40,
    tintColor: '#4FEA0B'
  },
  numberItem: {
    fontWeight: 'bold'
  }
})
