import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  itemsView: {
    flex: 0.4,
    alignContent: 'flex-start'
  },

  quantityView: {
    flex: 0.4,
    alignContent: 'flex-start',
    flexDirection: 'row'
  },
  priceView: {
    flex: 0.2,
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  contentView: {
    flexDirection: 'row',
    marginTop: 15
  },
  minusImage: {
    height: 15,
    width: 15,
    tintColor: 'red'
  },
  quantityText: {
    textAlign: 'left',
    fontWeight: 'bold',
    color: '#646068'
  },
  plusImage: {
    height: 15,
    width: 15,
    tintColor: '#55F117'
  },
  deleteImage: {
    height: 15,
    width: 15,
    tintColor: 'red',
    marginLeft: 5
  },
  textQuantityView: {
    flexDirection: 'row',
    flex: 0.8,
    justifyContent: 'space-between'
  },
  textNameAndPrice: {
    fontWeight: 'bold',
    color: '#646068'
  },
  insideQuantityView: {
    justifyContent: 'space-between',
    flex: 0.5,
    flexDirection: 'row'
  }
})
