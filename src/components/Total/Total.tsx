import React from 'react'
import { View, Text } from 'react-native'
import styles from '../Styles/TotalStyles'

type TotalProps = {
  totalPrice: number
  totalQuantity: number
}

const Total = (props: TotalProps) => {
  return (
    <View style={styles.seperateItem}>
      <View style={styles.itemsView}>
        <Text style={styles.textItem}>Total</Text>
      </View>
      <View style={styles.quantityView}>
        <View style={styles.centerQuantity}>
          <Text style={styles.textItem}>{props.totalQuantity}</Text>
        </View>
      </View>
      <View style={styles.priceView}>
        <Text style={styles.textItem}>$ {props.totalPrice}</Text>
      </View>
    </View>
  )
}

export default Total
