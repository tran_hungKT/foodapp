import React from 'react'
import { TouchableOpacity, Text, View, Image } from 'react-native'
import styles from '../Styles/MoveToPackageButtonStyles'
type MoveToPackageButtonProps = {
  onPress: () => void
  totalPrice: number
  totalItem: number
}

const MoveToPackageButton = (props: MoveToPackageButtonProps) => {
  return (
    <TouchableOpacity style={styles.mainContainer} onPress={props.onPress}>
      <Image source={require('../../Assets/cart.png')} style={styles.image} />
      <Text style={styles.numberItem}>
        ({props.totalItem} items): ${props.totalPrice}
      </Text>
    </TouchableOpacity>
  )
}

export default MoveToPackageButton
