import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import styles from '../Styles/OrderButtonStyles'
const OrderButton = () => {
  return (
    <TouchableOpacity style={styles.mainContainer}>
      <Image source={require('../../Assets/cart.png')} style={styles.image} />
      <Text style={styles.text}>Order</Text>
    </TouchableOpacity>
  )
}

export default OrderButton
