import React, { useState } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'

import styles from '../Styles/ListItemPackageStyles'
import { useDispatch } from 'react-redux'
import CartRedux from '../../Redux/RootReducer'
type Item = {
  id: string
  name: string
  price: number
  image: string
  quantity: number
}

type ListItemPackageProps = {
  packageItems: Item[]
}

const ListItemPackage = (props: ListItemPackageProps) => {
  const dispatch = useDispatch()

  const onMinusItem = (id: string) => {
    dispatch(CartRedux.decreaseQuantity(id))
  }

  const onPlusItem = (id: string) => {
    dispatch(CartRedux.increaseQuantity(id))
  }

  const onDeleteItem = (id: string) => {
    dispatch(CartRedux.deleteItem(id))
  }
  const listItem = props.packageItems.map((item, key) => {
    return (
      <View style={styles.contentView} key={key}>
        <View style={styles.itemsView}>
          <Text style={styles.textNameAndPrice}>{item.name}</Text>
        </View>
        <View style={styles.quantityView}>
          <View style={styles.insideQuantityView}>
            <View style={styles.textQuantityView}>
              <TouchableOpacity
                onPress={() => {
                  onMinusItem(item.id)
                }}
              >
                <Image
                  source={require('../../Assets/minus.png')}
                  style={styles.minusImage}
                  resizeMode={'contain'}
                />
              </TouchableOpacity>
              <Text style={styles.quantityText}>{item.quantity}</Text>
            </View>
            <TouchableOpacity onPress={() => onPlusItem(item.id)}>
              <Image
                source={require('../../Assets/plus.png')}
                style={styles.plusImage}
                resizeMode={'contain'}
              />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.priceView}>
          <Text style={styles.textNameAndPrice}>
            $ {item.price * item.quantity}
          </Text>
          <TouchableOpacity onPress={() => onDeleteItem(item.id)}>
            <Image
              source={require('../../Assets/remove.png')}
              style={styles.deleteImage}
              resizeMode={'contain'}
            />
          </TouchableOpacity>
        </View>
      </View>
    )
  })
  return <>{listItem}</>
}

export default ListItemPackage
