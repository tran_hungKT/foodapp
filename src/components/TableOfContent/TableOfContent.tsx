import React from 'react'
import { View, Text } from 'react-native'
import styles from '../Styles/TableOfContentStyles'
const TableOfContent = () => {
  return (
    <View style={styles.seperateItem}>
      <View style={styles.itemsView}>
        <Text style={styles.textItem}>Items</Text>
      </View>
      <View style={styles.quantityView}>
        <Text style={styles.textItem}>Quantity</Text>
      </View>
      <View style={styles.priceView}>
        <Text style={styles.textItem}>Price</Text>
      </View>
    </View>
  )
}

export default TableOfContent
