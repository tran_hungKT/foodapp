import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import CategoryScreen from '../Containers/Category'
import PackageScreen from '../Containers/Package'

const Stack = createStackNavigator()

const StackNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name='Category'
          component={CategoryScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name='Package'
          component={PackageScreen}
          options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default StackNavigator
