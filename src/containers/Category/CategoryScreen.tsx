import React, { Fragment, useState, useEffect } from 'react'
import { Text, TouchableOpacity, View } from 'react-native'
import styles from '../Styles/CategoryStyles'
import ListFood from '../../Components/ListFood'
import MoveToPackageButton from '../../Components/MoveToPackageButton'
import { StackNavigationProp } from '@react-navigation/stack'
import { useNavigation } from '@react-navigation/core'
import { useDispatch, useSelector } from 'react-redux'
import CartRedux from '../../Redux/RootReducer'
import RootState from '../../Types/RootState'
type RootStackParamsList = {
  Category: undefined
  Package: undefined
}

type ProfileScreenNavigationProp = StackNavigationProp<
  RootStackParamsList,
  'Category'
>

const Category = () => {
  const dispatch = useDispatch()
  const navigation = useNavigation<ProfileScreenNavigationProp>()
  const packageItem = useSelector((state: RootState) => state.cart)
  const [focusItem, setFocusItem] = useState<number>(0)
  const setOnFocusItem = (key: number) => {
    setFocusItem(key)
  }

  useEffect(() => {
    // Update the document title using the browser API
    dispatch(CartRedux.loadData())
  }, [])

  const listItem = packageItem.datas.map((data, key) => {
    return (
      <Fragment key={key}>
        <TouchableOpacity onPress={() => setOnFocusItem(key)}>
          {key == focusItem ? (
            <View style={styles.focusItem}>
              <Text style={styles.itemText}>
                {data.name} ({data.items.length})
              </Text>
            </View>
          ) : (
            <Text style={styles.itemText}>
              {data.name} ({data.items.length})
            </Text>
          )}
        </TouchableOpacity>
      </Fragment>
    )
  })

  const onAddItem = (item: object) => {
    dispatch(CartRedux.addItem(item))
  }

  return (
    <View style={styles.mainContainer}>
      <View style={styles.header}>
        <Text style={styles.headerText}>Category</Text>
      </View>
      <View style={styles.listCategory}>{listItem}</View>
      {packageItem.datas.length == 0 ? (
        <Text> {packageItem.errorMessage}</Text>
      ) : (
        <ListFood
          dataItems={packageItem.datas[focusItem].items}
          onPressItem={onAddItem}
        />
      )}
      <MoveToPackageButton
        onPress={() => navigation.navigate('Package')}
        totalPrice={packageItem.totalPrice}
        totalItem={packageItem.package.length}
      />
    </View>
  )
}
export default Category
