import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginHorizontal: 10
  },
  header: {
    alignContent: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0.1
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 16
  },
  listCategory: {
    flex: 0.06,
    flexDirection: 'row',
    borderBottomColor: '#ABAB98',
    borderBottomWidth: 0.5
  },
  itemText: {
    fontSize: 18,
    marginHorizontal: 5
  },
  focusItem: {
    marginHorizontal: 5,
    borderBottomColor: '#4FEA0B',
    borderBottomWidth: 1,
    flex: 0.6,
    alignContent: 'center'
  }
})
