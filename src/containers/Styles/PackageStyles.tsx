import { StyleSheet, StatusBar } from 'react-native'

export default StyleSheet.create({
  backgroundImage: {
    flex: 1
  },
  mainContainer: {
    flex: 1,
    marginHorizontal: 10
  },
  header: {
    flexDirection: 'row'
  },
  image: {
    height: 30,
    width: 30
  },
  backText: {
    fontWeight: 'bold',
    fontSize: 16,
    alignSelf: 'center'
  },
  mainContent: {
    marginTop: 10,
    marginHorizontal: 35,
    flex: 1
  },
  seperateItem: {
    flex: 0.1,
    flexDirection: 'row',
    borderBottomColor: '#B7C1B3',
    borderBottomWidth: 0.5
  },
  itemsView: {
    flex: 0.4,
    alignContent: 'flex-start'
  },
  textItem: {
    fontWeight: 'bold',
    fontSize: 16
  },
  quantityView: {
    flex: 0.4,
    alignContent: 'flex-start',
    flexDirection: 'row'
    // justifyContent: 'space-between'
  },
  priceView: {
    flex: 0.2,
    alignContent: 'flex-start',
    flexDirection: 'row'
  },
  listItemPackageView: {
    borderBottomColor: '#8C8C89',
    borderBottomWidth: 1.5,
    paddingBottom: 10
  },
  orderButtonView: {
    position: 'absolute',
    bottom: 40,
    right: 35
  }
})
