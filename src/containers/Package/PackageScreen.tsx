import React, { useState } from 'react'
import {
  Text,
  View,
  Image,
  SafeAreaView,
  TouchableOpacity,
  ImageBackground
} from 'react-native'

import { useSelector } from 'react-redux'
import styles from '../Styles/PackageStyles'
import ListItemPackage from '../../Components/ListItemPackage'
import TableOfContent from '../../Components/TableOfContent'
import Total from '../../Components/Total'
import RootState from '../../Types/RootState'
import OrderButton from '../../Components/OrderButton'
const PackageScreen = (props: any) => {
  const packageItem = useSelector((state: RootState) => state.cart)
  return (
    <ImageBackground
      source={require('../../Assets/bg.jpg')}
      style={styles.backgroundImage}
    >
      <SafeAreaView style={styles.mainContainer}>
        <TouchableOpacity
          style={styles.header}
          onPress={() => props.navigation.goBack()}
        >
          <Image
            source={require('../../Assets/back.png')}
            resizeMode={'contain'}
            style={styles.image}
          />
          <Text style={styles.backText}>Back</Text>
        </TouchableOpacity>
        <View style={styles.mainContent}>
          <TableOfContent />
          <View style={styles.listItemPackageView}>
            <ListItemPackage packageItems={packageItem.package} />
          </View>
          <Total
            totalPrice={packageItem.totalPrice}
            totalQuantity={packageItem.totalQuantity}
          />
        </View>
        <View style={styles.orderButtonView}>
          <OrderButton />
        </View>
      </SafeAreaView>
    </ImageBackground>
  )
}

export default PackageScreen
